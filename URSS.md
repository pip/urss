# URSS (Unidad Recargable Sustentable Sexy)

La URSS es una fuente de energía eléctrica modular.  Nuestro objetivo es
reciclar y reapropiarnos de componentes que de otra forma pasarían a
formar parte de la basura electrónica.

Es como un cargador portátil (o un _powerbank_) modular y con voltaje
regulable.

La URSS está compuesta por:

* Una o más baterías de litio-ion
* Un protector de descarga
* Un módulo de carga
* Un regulador de voltaje

## Por qué recuperar baterías

Las baterías de las computadoras portátiles de distintas formas, usos y
tamaños (_notebooks_, celulares, _tablets_, cámaras, etc.) puede que
sigan funcionando aún cuando los indicadores del equipo digan lo
contrario.

A veces es porque el circuito protector deja de funcionar o, si están
compuestas de varias "celdas" (baterías), algunas dejan de funcionar y
otras no, con lo que la computadora nos informa que se terminó la vida
útil del conjunto, pero no dice nada de sus componentes, que podemos
reutilizar.

Una de las razones es porque las baterías y otros componentes están
compuestas de minerales raros, que por un lado cuando los tiramos sin
más y no existen políticas públicas de reciclado, terminan filtrándose y
contaminando el medio ambiente.  Por otro lado, se suelen extraer en el
sur global a base de explotación de personas y otres animales y
destrucción ecológica en general.

La otra razón por la que reciclamos componentes es para demostrarnos que
somos capaces de crear nuestras tecnologías, con nuestras políticas y
objetivos y que no todas tienen que servir al control y la explotación,
sino que pueden acompañar y facilitar nuestra liberación mutua.

## Para qué se pueden usar

Las URSS tienen voltaje regulable con lo cual su uso es bastante
flexible: para cargar celulares, para encender LEDs, para alimentar
otros dispositivos, etc.

## Materiales

* Batería de _notebook_ en cualquier estado de desuso (de las 6 baterías
  que puede contener, algunas están rotas, otras no).

  Armamos las URSS uniendo tres. También se pueden usar de celular y
  otros tipos de baterías, mientras sean de Litio y funcionen en 3.7V.

* Un protector de descarga, los podemos reciclar de baterías de
  celular, con lo que al menos necesitaremos una batería de celular.
  Puede ser una batería rota (de las hinchadas).

* Cables de distintos colores (reciclados de una fuente de PC rota).
  **Nos gusta que tengan distintos colores.**

* Conectores de tipo Mólex (los sacamos de las fuentes de PC)

* Módulo de carga TP4056 (también los podemos comprar con protector)

* Módulo _Booster DC_ 5V

* Módulo _Step Up_ MT3608

* Cables USB a mini-USB

* Contenedores bonitos (tuppers, cajitas de pastillas, cualquier cosa
  plástica que proteja y aisle)

* Cinta aisladora, _film_

* Termocontraíble

## Herramientas necesarias

* Alicate

* Pinza

* Destornillador

* Soldadora

* Estaño

* Apoya-soldadora (puede ser un cacho de alambre, una lata, cualquier
  cosa que no se derrita y que sostenga la soldadora)

* Fibra indeleble (o marcador permanente)

## Rescatar las baterías

Rescatamos las baterías que no funcionan de las notebooks.  La carcaza
está sellada y no hay forma de abrirla.  Nosotrxs usamos una morsa, un
cincel (en realidad un destornillador plano grande) y un martillo de
goma.  Puede ser con una _dremel_ (torno de mano) con disco de corte,
aunque en nuestro experimento se rompió el disco antes que cortarse la
carcaza.

La mejor forma que encontramos fue empezar a cortar por la costura de la
carcaza e ir abriendo por el costado.  Las baterías están muy pegadas al
plástico y si no trabajamos con cuidado las podemos pinchar.

**Atención: Las baterías contienen Litio que es inflamable en contacto
con el aire.  Si las pinchamos como mínimo puede salir humo y como
máximo la batería puede explotar o prenderse fuego.  Este paso necesita
atención y cuidado.  Usemos herramientas de protección como guantes de
trabajo, anteojos de protección, etc.**

Una vez abierta la carcaza, vamos a encontrar 6 baterías de tipo 18650
unidas por chapitas metálicas y un circuito que es el controlador de
carga y descarga.  El circuito todavía no sabemos cómo reciclarlo, así
que por ahora lo guardamos.  También tiene uno o varios sensores
térmicos que se pueden usar para otros proyectos.

Las chapitas metálicas se pueden cortar para separar las baterías, pero
no conviene cortarlas del todo porque es más fácil soldar los cables
luego.  Si la chapa se despega de la batería todavía podemos soldar
limando un poco el contacto, para que pegue el estaño.

Las baterías vienen de distintos colores que no significan nada, pero
qué lindos, ¿no?  Las podemos combinar, siempre teniendo cuidado de que
tengan el mismo voltaje.

Necesitaremos ver cuáles funcionan y cuáles no.

## Probar las baterías

![](img/pack_de_baterias.jpg)

Con el _tester_ medimos el voltaje de cada una: tiene que ser mayor a
2-2,5 voltios, aunque algunas publicaciones recomiendan entre 3,5 y 4 V.
Esto es así porque las baterías indican su carga en un rango entre 2,5 y
3,7-4V.  Si la batería se descarga más de eso quiere decir que su vida
útil está agotada.  Si las usáramos correríamos el riesgo de hacerlas
hincharse (como las de celular cuando se rompen) o explotar, aunque hay
gente que las "revive", todavía no investigamos esta posibilidad.

Con una fibra indeleble escribimos el voltaje en la misma batería.  Las
baterías de descarte se pueden guardar para enviar a reciclar (estimamos
que las tiran en el camino del Buen Ayre...)

## Recuperar el protector de descarga

![](img/protector_de_descarga.jpg)

Las baterías de Litio indican su estado de carga según el voltaje que
son capaces de entregar.  Una batería está cargada al 100% cuando
entrega 4V.  Una batería está descargada al 0% cuando entrega 2-2,5V.
Más bajo que eso es peligroso porque la batería se fuerza y puede
explotar.

Para eso las baterías tienen un protector de descarga, que es un
circuito que mide el voltaje de las baterías y corta la corriente cuando
detecta que llegan al voltaje mínimo.  El circuito solo vuelve a
funcionar cuando detecta carga, o sea cuando se invierte la tensión y la
batería recibe corriente en lugar de entregarla.

Podemos comprar algunos o reciclarlos de las baterías de celular, que ya
lo tienen incorporado.  Incluso existen módulos de carga TP4056 que
incluyen protector de descarga.

Las baterías de celular son chatas y en uno de los extremos hay una tira
de plástico, generalmente negro, con entre tres y cuatro conectores.
Nos interesan solo los que estén marcados como positivos (+) y negativo
(-), los otros son de control (y todavía no sabemos para qué los podemos
usar).

Para recuperar el protector, con una pinza de punta lo sostenemos y
movemos hacia los costados hasta que se afloje.  Luego podemos separarlo
y vamos a encontrar que está unido a la batería por dos chapitas.
Cortamos cada una individualmente con un alicate para separar el
protector de la batería.  Es muy probable que al separar el protector de
la batería se corte una de las chapitas y dejemos de tener acceso, pero
no hay problema, porque no la necesitamos.

Luego, de las dos chapitas, tenemos que identificar cual es la positiva
y cual la negativa.  Si medimos en la batería los pines donde estuvieron
conectados los del protector, podemos identificar cuál es cuál.  En
nuestra experiencia, el pin central es negativo y el del costado es
positivo.

Para probar el protector, soldamos tres cables.  Uno a cada pin externo
marcados como positivo y negativo y un tercero al pin interno del
negativo.  Como el circuito está puenteado por positivo y solo se
protege la tensión negativa, con tres cables es suficiente.  (Es
probable que al separar el protector de la batería este conector interno
se corte.)


## Armar el paquete de baterías

Elegimos tres pilas con el mismo voltaje.  Si no tienen el mismo
voltaje, pero miden más de 2V, podemos cargarlas individualmente con el
TP4056 y luego medirlas de vuelta.

Ubicamos las tres pilas "acostadas". Vamos a **conectarlas en
paralelo**: los tres lados positivos juntos por un lado y los tres
negativos juntos por el otro.  Esto es así para que la suma de las tres
pilas entregue el mismo voltaje (3.7V) pero sumen su capacidad de carga,
es decir 3x1.5A nos da un paquete de baterías de 4.5A (o 4500mA).

Mantenemos las pilas unidas envolviéndolas en cinta aisladora o _film_
plástico.  Si se les ocurre un metodo más sexy, pueden probarlo y
sumarlo a la documentación.  Sabemos que existe termocontraíble de este
tamaño e incluso transparente, pero es caro aun con los 12cm. que
necesitaríamos.

Unimos las baterías entre sí soldándolas con cables.  Recomendamos usar
colores que permitan la identificación como negro para negativo y rojo u
otro color para positivo.  Si cuando separamos las baterías dejamos las
chapitas pegadas es más fácil soldar, si las sacamos o se salieron hay
que limar un poco las terminales de la batería para que el estaño
agarre.

Soldamos un cable de color para el positivo y uno negro para el negativo
y los dejamos sueltos.

![](img/pack_de_baterias.jpg)

Unimos el negativo interno del protector al negativo de la batería.
Protegemos la unión con termocontraíble.

Elegimos un conector mólex pinchudo de los que se usan en los gabinetes
de computadoras de escritorio.  Los mólex hoyudos, que son los más
comunes, los usamos para los módulos que van conectados a la batería.

**Atención: No usamos términos biologicistas para referirnos a los
tipos de conectores, pero como siguen existiendo, decimos _pinchudo_ y
_hoyudo_.  Si se les ocurren otros términos son bienvenidos :)**

Como los mólex tienen cuatro pines y vamos a usar solo dos, elegimos el
par rojo-negro y cancelamos el par amarillo-negro cortándolos al ras.
Usamos estos conectores porque los comprados son difíciles de armar,
necesitan herramientas especiales y caras y manualmente nunca quedan
bien.  Si se les ocurren conectores reciclados de algún
dispositivo/cable fácilmente conseguibles (por ejemplo, no los USB
porque los hoyudos son más difíciles), ¡avisen!

Entonces, soldamos el cable positivo del pack de baterías al rojo del
mólex y el negativo al negativo externo del protector. Para que quede
prolijo, usamos tubitos termocontraíbles.  El proceso es:

* Cortar 3 tubitos de termocontraíble de 1-2cm y pasarlos por los cables
  antes de soldar.

* Pelar los cables del pack y del protector, medio centrímetro al menos.

* Cortar el par amarillo y negro del mólex pinchudo al ras para
  cancelarlos

* Cortar el par rojo y negro del mólex pinchudo al menos a 3cm y
  pelarlos

* Para unir los cables antes de soldarlos, abrimos los hilos de cobre
  como los dedos de una mano, los entrelazamos con el color que
  corresponda y giramos para que queden agarrados.  Luego pintamos con
  estaño para que queden fijos.

  Dar un tironcito a cada uno para probar que hayan quedado bien
  unidos.

  Unimos negativo de la batería con negativo interno del protector.
  Unimos el positivo del mólex con el positivo de la batería y el
  negativo con el negativo externo del protector.

* Una vez soldados los cables, deslizar los termocontraíbles para cubrir
  las partes soldadas y con el borde de la soldadora (no la punta!)
  pasar suavemente por el termocontraíble para que se contraiga.  Si el
  termocontraíble es muy fino, puede ser que se haya contraído mientras
  soldábamos los cables :/

¡Ya está listo el pack de baterías!  El pack tiene una salida igual al
voltaje individual de las baterías (nominalmente 3.7V) y una intensidad
de corriente combinada (nominalmente 1.5A * 3 baterías = 4.5A)

Luego se puede cubrir todo el pack con cinta aisladora, termocontraíble,
film...

![](img/pack_de_baterias_con_protector.jpg)

## Armar el módulo de carga

![](img/modulo_de_carga.jpg)

Para armar el módulo de carga, seguir los mismos pasos para soldar
cables a un conector mólex, pero esta vez en lugar de pinchudo es
hoyudo.  Usamos los hoyudos porque con menos comunes.  Una vez terminado
este paso, hay que soldar los cables a las terminales del módulo TP4056.
Soldados el cable positivo (el rojo del mólex) a la terminal positiva
del TP4056 y el negativo a la terminal negativa.

El módulo de carga permite cargar el pack de baterías utilizando un
cargador común de celular.  En nuestras pruebas, tarda 5 horas en
terminar de cargar.  Tiene un LED rojo para indicar que está cargando y
uno verde para indicar que la carga está completa.

## Armar el módulo de descarga

Hacemos los mismos pasos que el módulo de carga, con mólex pinchudo
incluso, en el módulo Booster DC 5V.  Este módulo permite cargar un
celular o una Raspberry, aunque el voltaje de salida es de 5V y la
potencia baja de 4.5A a 0.6A (600mA), con lo que una Raspberry puede
estar muy justa para alimentarse.

## Armar el módulo de descarga regulable

Como el módulo booster 5V es bastante limitado (¡aunque tiene un
conector USB hoyudo!) en cuanto a potencia y voltaje y además un poco
caro en comparación a otros módulos, usamos un módulo step-up MT3608 que
permite un voltaje de entrada de 3.7V y es capaz de elevarlo hasta 28V.
Además, puede entregar 2A, con lo que podemos alimentar dispositivos que
necesiten más potencia.

Para la parte de entrada, marcadas en el módulo como VIN+ y VIN-
(entrada positiva y negativa), repetimos los pasos para el módulo de
descarga y de carga normal.

Para la parte de salida, marcadas como VOUT+ y VOUT-, volvemos a usar un
mólex pinchudo y lo soldamos como en el pack de baterías.

Con esto podemos armar conectores de descarga usando un mólex hoyudo y
el conector que queramos, por ejemplo un plug de 2.1mm para alimentar
routers TP-Link, un conector mini-USB pinchudo para alimentar
Raspberries, etc.

¡Es importante tener cuidado con el voltaje o podemos quemar las
dispositivas!  Para alimentar un router usando el módulo jack 2.1mm, hay
que regular el MT3608 en 9V o 12V, según lo que diga la etiqueta.  Para
una Raspberry, hay que regularlo en 5.1V.  Las tiras de LED suelen
funcionar en 12V.

## Regular el módulo MT3608

Con un destornillador plano chiquito y un téster podemos regular el
MT3608.

* Conectar la batería al módulo para alimentarle voltaje

* Medir voltaje con el téster en las terminales VOUT del módulo.  Hay
  que tener cuidado con las puntas del téster porque si las cruzamos con
  otro componente lo podemos quemar.

* Con el destornillador ir girando el tornillito del cubo azul del
  módulo hasta que logramos el voltaje que queremos (es una resistencia
  regulable).  A veces toma muchas vueltas en empezar a regular, hay que
  tenerle paciencia.

* Podemos escribirle el voltaje con un marcador indeleble aunque siempre
  conviene testear antes de usarlo por si se mueve solo (o alguien lo
  movió y no avisó ¬¬)

## Proteger el pack y los módulos

![](img/modulo_de_carga_protegido.jpg)

Para proteger el pack y los módulos podemos usar:

* Tuppers chiquitos para el pack

* Cajitas de tictac para los módulos (aunque no son muy resistentes,
  tienen una puertita para sacar los cables)

* Carcazas recicladas de viejas dispositivas ya difuntas

* ...

* Cajitas de cartón (?) o MDF.

## Licencia

Taller Libre de Electrónica (DTL! + PIP) - 2018-2019
<https://tallerlibre.ptga.com.ar/>

Esta guía se libera bajo la Licencia de Producción de Pares
<https://endefensadelsl.org/ppl_deed_es.html>
