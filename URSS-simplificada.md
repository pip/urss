# URSS (Unidad Recargable Sustentable Sexy) versión simplificada

La URSS es una fuente de energía eléctrica.  Nuestro objetivo es
reciclar y reapropiarnos de componentes que de otra forma pasarían a
formar parte de la basura electrónica.

Es un cargador portátil (o un _powerbank_) que podemos usar para cargar
celulares.

## Por qué recuperar baterías

Las baterías de las computadoras portátiles de distintas formas, usos y
tamaños (_notebooks_, celulares, _tablets_, cámaras, etc.) puede que
sigan funcionando aún cuando los indicadores del equipo digan lo
contrario.

A veces es porque el circuito protector deja de funcionar o, si están
compuestas de varias "celdas" (baterías), algunas dejan de funcionar y
otras no, con lo que la computadora nos informa que se terminó la vida
útil del conjunto, pero no dice nada de sus componentes, que podemos
reutilizar.

Una de las razones es porque las baterías y otros componentes están
compuestas de minerales raros, que por un lado cuando los tiramos sin
más y no existen políticas públicas de reciclado, terminan filtrándose y
contaminando el medio ambiente.  Por otro lado, se suelen extraer en el
sur global a base de explotación de personas y otres animales y
destrucción ecológica en general.

La otra razón por la que reciclamos componentes es para demostrarnos que
somos capaces de crear nuestras tecnologías, con nuestras políticas y
objetivos y que no todas tienen que servir al control y la explotación,
sino que pueden acompañar y facilitar nuestra liberación mutua.

## Para qué se pueden usar

Las URSS simplificadas no se pueden regular pero se pueden usar para
alimentar celulares, _tablets_, Raspberries...

## Materiales

* Batería de _notebook_ en cualquier estado de desuso (de las 6 baterías
  que puede contener, algunas están rotas, otras no).

  Armamos las URSS uniendo tres. También se pueden usar de celular y
  otros tipos de baterías, mientras sean de Litio y funcionen en 3.7V.

* Cables de distintos colores (reciclados de una fuente de PC rota).
  **Nos gusta que tengan distintos colores.**

* Módulo de carga TP4056 **con protector** y conector USB:
  <https://www.nubbeo.com.ar/cargador-bateria-litio-con-proteccion-tp4056-1a-microusb-arduino-nubbeo-730601008xJM>

* Cables USB a mini-USB

* Contenedores bonitos (tuppers, cajitas de pastillas, cualquier cosa
  plástica que proteja y aisle)

* Cinta aisladora, _film_

## Herramientas necesarias

* Alicate

* Pinza

* Destornillador

* Soldadora

* Estaño

* Apoya-soldadora (puede ser un cacho de alambre, una lata, cualquier
  cosa que no se derrita y que sostenga la soldadora)

* Fibra indeleble (o marcador permanente)

## Rescatar las baterías

Rescatamos las baterías que no funcionan de las notebooks.  La carcaza
está sellada y no hay forma de abrirla.  Nosotrxs usamos una morsa, un
cincel (en realidad un destornillador plano grande) y un martillo de
goma.  Puede ser con una _dremel_ (torno de mano) con disco de corte,
aunque en nuestro experimento se rompió el disco antes que cortarse la
carcaza.

La mejor forma que encontramos fue empezar a cortar por la costura de la
carcaza e ir abriendo por el costado.  Las baterías están muy pegadas al
plástico y si no trabajamos con cuidado las podemos pinchar.

**Atención: Las baterías contienen Litio que es inflamable en contacto
con el aire.  Si las pinchamos como mínimo puede salir humo y como
máximo la batería puede explotar o prenderse fuego.  Este paso necesita
atención y cuidado.  Usemos herramientas de protección como guantes de
trabajo, anteojos de protección, etc.**

Una vez abierta la carcaza, vamos a encontrar 6 baterías de tipo 18650
unidas por chapitas metálicas y un circuito que es el controlador de
carga y descarga.  El circuito todavía no sabemos cómo reciclarlo, así
que por ahora lo guardamos.  También tiene uno o varios sensores
térmicos que se pueden usar para otros proyectos.

Las chapitas metálicas se pueden cortar para separar las baterías, pero
no conviene cortarlas del todo porque es más fácil soldar los cables
luego.  Si la chapa se despega de la batería todavía podemos soldar
limando un poco el contacto, para que pegue el estaño.

Las baterías vienen de distintos colores que no significan nada, pero
qué lindos, ¿no?  Las podemos combinar, siempre teniendo cuidado de que
tengan el mismo voltaje.

Necesitaremos ver cuáles funcionan y cuáles no.

## Probar las baterías

![](img/pack_de_baterias.jpg)

Con el _tester_ medimos el voltaje de cada una: tiene que ser mayor a
2-2,5 voltios, aunque algunas publicaciones recomiendan entre 3,5 y 4 V.
Esto es así porque las baterías indican su carga en un rango entre 2,5 y
3,7-4V.  Si la batería se descarga más de eso quiere decir que su vida
útil está agotada.  Si las usáramos correríamos el riesgo de hacerlas
hincharse (como las de celular cuando se rompen) o explotar, aunque hay
gente que las "revive", todavía no investigamos esta posibilidad.

Con una fibra indeleble escribimos el voltaje en la misma batería.  Las
baterías de descarte se pueden guardar para enviar a reciclar (estimamos
que las tiran en el camino del Buen Ayre...)

## Armar el paquete de baterías

Elegimos tres pilas con el mismo voltaje.  Si no tienen el mismo
voltaje, pero miden más de 2V, podemos cargarlas individualmente con el
TP4056 y luego medirlas de vuelta.

Ubicamos las tres pilas "acostadas". Vamos a **conectarlas en
paralelo**: los tres lados positivos juntos por un lado y los tres
negativos juntos por el otro.  Esto es así para que la suma de las tres
pilas entregue el mismo voltaje (3.7V) pero sumen su capacidad de carga,
es decir 3x1.5A nos da un paquete de baterías de 4.5A (o 4500mA).

Mantenemos las pilas unidas envolviéndolas en cinta aisladora o _film_
plástico.  Si se les ocurre un metodo más sexy, pueden probarlo y
sumarlo a la documentación.  Sabemos que existe termocontraíble de este
tamaño e incluso transparente, pero es caro aun con los 12cm. que
necesitaríamos.

Unimos las baterías entre sí soldándolas con cables.  Recomendamos usar
colores que permitan la identificación como negro para negativo y rojo u
otro color para positivo.  Si cuando separamos las baterías dejamos las
chapitas pegadas es más fácil soldar, si las sacamos o se salieron hay
que limar un poco las terminales de la batería para que el estaño
agarre.

Soldamos un cable de color para el positivo y uno negro para el negativo
y los dejamos sueltos.

Entonces, soldamos el cable positivo del pack de baterías al positivo
del módulo de carga y el negativo con el negativo.

* Pelar los cables del pack y del protector, medio centrímetro al menos.

* Unimos negativo de la batería con el negativo del módulo y el positivo
  con el positivo.  Aplicamos un punto de estaño en cada uno para
  dejarlos firmes.

¡Ya está listo el pack de baterías!  El pack tiene una salida igual al
voltaje individual de las baterías (nominalmente 3.7V) y una intensidad
de corriente combinada (nominalmente 1.5A * 3 baterías = 4.5A)

Luego se puede cubrir todo el pack con cinta aisladora, termocontraíble,
film...


## Proteger el pack y el módulo

Para proteger el pack y los módulos podemos usar:

* Tuppers chiquitos para el pack

* Cajitas de tictac para los módulos (aunque no son muy resistentes,
  tienen una puertita para sacar los cables)

* Carcazas recicladas de viejas dispositivas ya difuntas

* ...

* Cajitas de cartón (?) o MDF.

## Licencia

Taller Libre de Electrónica (DTL! + PIP) - 2018-2019
<https://tallerlibre.ptga.com.ar/>

Esta guía se libera bajo la Licencia de Producción de Pares
<https://endefensadelsl.org/ppl_deed_es.html>
